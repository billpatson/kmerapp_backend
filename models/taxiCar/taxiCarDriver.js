const mongoose = require("mongoose");

var geoSchema = mongoose.Schema(
  {
    /*type:{
        type: String,
        enum: 'Point',
        default: 'Point'
    },*/
    coordinates: {
      type: [Number],
      index: "2dsphere",      
    },
  },
  { _id: false }
);

const TaxiDriverLocationSchema = mongoose.Schema(
  {
    userId: { type: String },
    //userData:{type:Object},
    /*TaxiDriverInfo:{
        DriverId:{ type: String },
        VehiculePlateNumber: { type: String },

    },*/
    coordinate: geoSchema,
    // socketId:{ type: String },
    /*createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now }*/
  },
  { collection: "TaxiDriverLocations" }
);

const TaxiDriverLocation = mongoose.model("TaxiDriverLocation",TaxiDriverLocationSchema);

module.exports = TaxiDriverLocation;
