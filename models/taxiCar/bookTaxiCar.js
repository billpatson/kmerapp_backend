const mongoose = require("mongoose");

var geoSchema = mongoose.Schema(
  {
    coordinates: {
      type: [Number],
      index: "2dsphere",      
    },
  },
  { _id: false }
);

const bookTaxiCars = mongoose.Schema(
  {
    userId: { type: String },
    //userData:{type:Object},
    //coordinate: geoSchema,
    // socketId:{ type: String },
    taxiBookingDetails:{type:Object},
    taxiDriverUserID:{type:String},
    taxiPassengerUserID:{type:String},
    taxiDriverData:{type:Object},
    taxiDriverDataPositionData:{ type: Object },
    passengerData:{type:Object},
    resquestType:{type: String},
    createdAt: { type: Date, default: Date.now },
	  updatedAt: { type: Date },
    bookingStatus:{type: String},
    taxiBookingID:{type: String},
  },
  { collection: "bookTaxiCar" }
);



const saveTaxiBooking = mongoose.Schema({
  taxiPassengerData : { type: Object },
  taxiDriverData : { type: Object },
  taxiBookingDetails:{type:Object},
  taxiModeType:{type:String}, // Mototaxi  or TaxiCar
  taxiDriverUserID:{type:String},
  taxiPassengerUserID:{type:String},
  taxiBookingID:{type:String},
  taxiBookingStatus:{type:String},
  taxiDriverDataPositionData:{ type: Object },
  createdAt: { type: Date, default: Date.now },

},
{ collection: "savedTaxiBooking" }
);
const bookTaxiCarSchema = mongoose.model("bookTaxiCarDriver",bookTaxiCars);
const saveTaxiBookingSchema = mongoose.model("saveTaxiBookings",saveTaxiBooking);

module.exports = { BookTaxiCar:bookTaxiCarSchema, SaveTaxiBooking:saveTaxiBookingSchema };
