const { Number } = require("mongoose");
const mongoose = require("mongoose");
const otpSchema = new mongoose.Schema({
    otpCode: { type: Number },
    email: { type: String },
    phoneNumber: { type: String },
    resetTryArray: [
        {
            email: { type: String },
            phoneNumber: { type: String },
        }
    ],
    //resetTryCount: { type: Number },
},
    { collection: "otp" }
);
const CustomOtp = mongoose.model("CustomOtp", otpSchema);

module.exports = CustomOtp;