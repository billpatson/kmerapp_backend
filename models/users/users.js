const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
    userName:{ type: String,},//    userName:{ type: String,required:true},

    password:{ type: String},
    lastName:{type: String},
    firstName:{type: String},
    email:{type: String},
    phoneNumber:{type: String},
    identityCardNumber:{type: String},
    userPhoto:{type: String},
    vehiculedata:{type: Object},
    familyData:{type: Object},
    userRole:{ type: Object},
    /*userTaxiBooking: {type: Object},
    userAllTaxiBooking: {type: Object},
    userTaxiBooking: {type: Object},
    userAllTaxiBooking: {type: Object}*/


},
{collection:"users"}
);
const User = mongoose.model("User", userSchema);

module.exports = User;