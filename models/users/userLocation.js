const mongoose = require("mongoose");

var geoSchema = mongoose.Schema(
  {
    /*type:{
        type: String,
        enum: 'Point',
        default: 'Point'
    },*/
    coordinates: {
      type: [Number],
      index: "2dsphere",
      //default: [0, 0]
    },
  },
  { _id: false }
);
/*var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;*/

const usersLocationSchema = mongoose.Schema(
  {
    userData:{type: Object},
    userName: { type: String },
    userId: { type: String },
    coordinate: geoSchema,
  },
  { collection: "usersLocationsCollections" }
);

const UsersLocation = mongoose.model("usersLocation", usersLocationSchema);

module.exports = UsersLocation;
