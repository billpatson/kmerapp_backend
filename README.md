Backend for the kmerApp App:

kmerApp is an app which provides the most important monitor data in a mobile app.

draft:
to control: 

to control: 

to control: 

to control: 

to control: 

Technical stack
bcrypt: "^5.0.1"

body-parser: "^1.18.3"

express: "^4.17.2"

jsonwebtoken: "^8.5.1"

mongoose: "5.4.17"

pg: "^8.7.3"

How to install:
Environment required:

1- Nodejs installation: curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - sudo apt-get install -y nodejs

2- Server Installation npm init -y

3- Edit index.js

add following src in index.js to check if your Server work fine

const http = require('http');

const hostname = '127.0.0.1'; const port = 3000;

const server = http.createServer((req, res) => { res.statusCode = 200; res.setHeader('Content-Type', 'text/plain'); res.end('Hello World'); });

server.listen(port, hostname, () => { console.log(Server running at http://${hostname}:${port}/); });

Now, run your web server using node app.js. Visit http://localhost:3000 and you will see a message saying "Hello World".

3- install yarn :

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update

sudo apt install yarn

4- install nodemon:

npm install -g nodemon

5- Express: npm install express --save

6- install MongoDB:

6.1- Installation:

wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -

echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list

sudo apt-get update

sudo apt-get install -y mongodb-org

sudo systemctl start mongod sudo systemctl enable mongod

6.2 - MongoDB Usage: show dbs // to show all database

use <databaseName> //to create a new Database

db.<collectionName>.insert({......}) //to create a new collection and insert data into

Usage:
1- clone the Repository

2- open a Terminal, navigate to the clone Repository and run yarn install or run npm install

3- run the Script with ---> nodemon index.js

Errors Handling:
Error: listen EADDRINUSE: address already in use :::4000
Solution:

sudo lsof -i :4000 // to get the PID Number for the Process which listen at the Port 4000

sudo kill -9 PID // change PID with the Process Number

restart the Server with ---> nodemon index.js