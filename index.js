const fs = require('fs');

const SSL = {
    credentials: {

        /*key: fs.readFileSync(`${__dirname}/certificates/privkey.pem`, 'utf8'),//fs.readFileSync('certificates/private.pem', 'utf8'),
        cert: fs.readFileSync(`${__dirname}/certificates/cert.pem`, 'utf8'),//fs.readFileSync('certificates/cert.pem', 'utf8'),
        ca: [
            fs.readFileSync(`${__dirname}/certificates/chain.pem`, 'utf8'),//fs.readFileSync('certificates/chain.pem', 'utf8')
        ]*/
        key: fs.readFileSync(`/etc/letsencrypt/live/pt-api-dev-ne.northeurope.cloudapp.azure.com/privkey.pem`, 'utf8'),
        cert: fs.readFileSync(`/etc/letsencrypt/live/pt-api-dev-ne.northeurope.cloudapp.azure.com/cert.pem`, 'utf8'),
        ca: [
            fs.readFileSync(`/etc/letsencrypt/live/pt-api-dev-ne.northeurope.cloudapp.azure.com/chain.pem`, 'utf8'),
        ]
    }
};


const express = require('express');
const app = express();
const http = require('http').createServer(app);
const https = require('https').createServer(SSL.credentials, app);
const socketIO = require('socket.io')(https);

const http_port = 3001;
const https_port = 3000;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const userRouter = require('./routes/users/users');
const authRouter = require('./routes/auth');
const mongoDbConnectionString = require('./config/mongodb');
const taxiDriverRouter = require('./routes/taxiCar/taxiDriver');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    next();
});

app.use("/api/auth", authRouter);
app.use("/api/users", userRouter);

app.use("/api/taxiCar", taxiDriverRouter);
app.get('/toto', (req, res) => res.send('Hello World!'));



mongoose
    .connect(mongoDbConnectionString, {
        useNewUrlParser: true,
        //useUnifiedTopology: true,
        useCreateIndex: true,
    })
    .then((result) => {
        console.log("Connected to Mongodb");


        /*http.createServer(app).listen(http_port, () => {
            console.log(" http Server is listening on port: " + http_port);
        })
        https.createServer(SSL.credentials, app).listen(https_port, () => {
            console.log("https Server is listening on port: " + https_port);
        })*/
        /*https.createServer(SSL.credentials, app).listen(https_port, () => {
            console.log("https Server is listening on port: " + https_port);
        })*/

        app.get('/', (req, res) => {
            res.status(200).send('Hello World!....ca va  lol new toto hi');
        })

        socketIO.on('connection', function (socket) {
            socket.on('toto', function (msg) {
                console.log('message: ' + msg);
                socket.broadcast.emit("toto", 'hello bonjour boy' + msg)

                socket.emit("tchop", 'bonjour boy' + msg)


            });

            socket.on('taxiCarRequest', function (msg) {
                console.info('message: ' + msg);
                socket.broadcast.emit("taxiCarRequest", msg)

                //socket.broadcast.emit("toto", msg)


            });

            socket.on('taxiCarRequest', data =>{
                console.info('hello mydata =',data.passenger_userID);
                socket.broadcast.emit("taxiCarRequest", data.passenger_userID)

            })
            socket.on('taxiCardriversLocation', function (position) {
                socket.broadcast.emit('receiveTaxiCardriversLocation',position);
                console.info('position=',position);
            })
        });

        https.listen(https_port, function () {
            console.log("https Server is listening on port: " + https_port);

        })

    })
    .catch((err) => {
        console.error(err);
    });
