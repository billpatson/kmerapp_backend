const request = require('./requesthelper');
var axios = require('axios');

async function getTimeBetween2Coordinates(origin_latitude, origin_longitude, destination_latitude, destination_longitude) {
    try {

        let API_KEY = "AIzaSyAElGBYi3ye4mK2bduHM_Nb4SC2JaMcgS8";
        var url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin_latitude},${origin_longitude}&destinations=${destination_latitude},${destination_longitude}&units=imperial&key=${API_KEY}`;

        const {data:response} = await axios.get(url) //use data destructuring to get data from the promise object
        return response
    } catch (err) {

    }

}
function getTimeBetween2Coordinatesss(origin_latitude, origin_longitude, destination_latitude, destination_longitude) {

    let API_KEY = "AIzaSyAElGBYi3ye4mK2bduHM_Nb4SC2JaMcgS8";
    /*let duration = '';

    request.get("https://maps.googleapis.com/maps/api/distancematrix/json?")
        .query({
            origins:origin_latitude + "," + origin_longitude,
            destinations: destination_latitude + "," + destination_longitude,
            mode:'driving',
            key:"AIzaSyAElGBYi3ye4mK2bduHM_Nb4SC2JaMcgS8"
        })
        .finish((err, res) =>{

            duration = res.body.rows[0].elements[0].duration.text
            console.info('res.body.rows[0].elements[0].duration.text =',res.body.rows[0].elements[0].duration)
        })
        console.log('duration=',duration);
    return duration;*/
    var config = {
        method: 'get',
        url: `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin_latitude},${origin_longitude}&destinations=${destination_latitude},${destination_longitude}&units=imperial&key=${API_KEY}`,
        headers: {}
    };
    let myvalue = {};
    //return axios(config).then(response => response.data.rows[0].elements[0].duration)
    // create a promise for the axios request
    /* const promise = await axios(config)
 
     // using .then, create a new promise which extracts the data
     const dataPromise = promise.then((response) => response.data.rows[0].elements[0].duration)
 
     // return it
     return dataPromise*/


    var url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin_latitude},${origin_longitude}&destinations=${destination_latitude},${destination_longitude}&units=imperial&key=${API_KEY}`;
    //const response = await axios.get(url)
    return axios.get(url).then(response => response.data.rows[0].elements[0].duration.text)
    //return response.data.rows[0].elements[0].duration.text
    /*return axios(config)
    .then(function (response) {
      console.log('hvbsibivs',response.data);
      let test = response.data.rows[0].elements[0].duration.text;
      myvalue = response.data.rows[0].elements[0].duration
      console.log('tototo myvalue',myvalue);
    })
    .catch(function (error) {
      console.log(error);
    });*/


}


module.exports = {
    getTimeBetween2Coordinates: (param1, param2, param3, param4) => getTimeBetween2Coordinates(param1, param2, param3, param4)
}