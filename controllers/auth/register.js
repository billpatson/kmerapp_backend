const User = require('../../models/users/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.register = async (req, res, next) => {

    console.info('req.body=', req.body);

    try {
        const { 
            userName, 
            password, 
            userRole,
            lastName, 
            firstName,
            email,
            phoneNumber,
            identityCardNumber,
            userPhoto,
            vehiculedata,
            familyData 
        } = req.body;
        /*console.info('userName=', userName);
        console.info('password=', password);
        console.info('userRole=', userRole);*/


        if (await User.findOne({ userName })) {
            const error = new error(`An Account with the Name ${userName} already exists`);
            error.statusCode = 409;
            console.info('error',error)

            throw error;

        }

        const hashedPassword = await bcrypt.hash(password, 12);
        const user = new User({
            userName: userName,
            password: hashedPassword,
            userRole: userRole,
            lastName:lastName, 
            firstName:firstName,
            email:email,
            phoneNumber:phoneNumber,
            identityCardNumber:identityCardNumber,
            userPhoto:userPhoto,
            vehiculedata:vehiculedata,
            familyData:familyData
        });

        user.save()
        .then(userData =>{
            res.status(200).json({
                registerStatus: "User added successfully",
                userData: userData
              });
        })
        .catch(err={

        })
        

        /*const result = await user.save();
        res.send({ userData: result });*/
    } catch (err) {
        next(err);
    }
}

