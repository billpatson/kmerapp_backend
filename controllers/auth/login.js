const User = require('../../models/users/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtSecret = require('../../config/jwtSecret');

exports.login = async(req,res,next) =>{

    console.info('req.body=',req.body);

    try{
        const {userName , password } = req.body;
        console.info('my userName=',req.body.userName);

        console.info('userName=',userName);
        console.info('password=',password);

        const user = await User.findOne({ userName });
        if(user){
            const isPasswordCorrect = await bcrypt.compare(password, user.password);
            if(isPasswordCorrect){
                const token = jwt.sign({userId: user._id}, jwtSecret);
                console.info('token= ',token);
                console.info('userData= ',user);

                return res.json({token, userData:user, userId:user._id})
            }
            const error = new Error(`Password does not match userName ${userName}`);
            error.statusCode = 401;
            throw error;
        }
        const error = new Error(`This userName ${userName} does not exist`);
        error.statusCode = 401;
        throw error;
    }catch(err){
        next(err);
    }
}

exports.getAllUsers = async (req, res, next) => {
    console.info('bjr de tonton billy');

    try {
        await User.find()
            .then((user) => {
                console.info('Users Infos:',user);
                res.status(201).json({ user })
            })
            .catch((error) => res.status(400).json({ error }));
    } catch (err) {
        next(err);
    }

}