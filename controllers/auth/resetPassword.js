const User = require('../../models/users/users');
const CustomOtp = require('../../models/users/resetPassword');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer')

var otpGenerator = function (length) {
    return Math.floor(Math.pow(10, length - 1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1));
}

exports.resetPassword = async (req, res, next) => {
    try {
        console.info('req.body',req.body)
        const hashedPassword = await bcrypt.hash(req.body.password, 12);
        console.info('hashedPassword',hashedPassword)
        User.updateOne({ email: req.body.email, phoneNumber: req.body.phoneNumber },
            {
                $set: {
                    password: hashedPassword
                }
            })
            .then(() => {
                res.status(200).json({ message: 'password updated!' })

            })
            .catch(error => {
                console.info('error',error)
                res.status(400).json({ error })
                
            });

    } catch (err) {

    }
}
exports.sendOtpCode = async (req, res, next) => {
    try {
        const checkOtpCode = await CustomOtp.find({ email: req.body.email, phoneNumber: req.body.phoneNumber, otpCode: req.body.otpCode });

        if (checkOtpCode) {
            return res.status(200).json({ message: "otpCode ok" });

        } else {
            return res.status(200).json({ message: "Please contact our Service" });
        }
    } catch (err) {

    }
}
exports.checkIfEmailandPhoneNumberExist = async (req, res, next) => {
    console.log('bjr 111 ')

    try {
        console.log('bjr ')
        const checkifEmailExist = await User.findOne({ email: req.body.email });
        const checkifPhoneNumberExist = await User.findOne({ phoneNumber: req.body.phoneNumber });

        console.info('checkifEmailExist', checkifEmailExist)
        console.info('checkifPhoneNumberExist', checkifPhoneNumberExist)

        const userEmail = await User.findOne({ email: req.body.email, phoneNumber: req.body.phoneNumber });
        const userphoneNumber = await User.findOne({ phoneNumber: req.body.phoneNumber, email: req.body.email });

        if (userEmail && userphoneNumber) {
            console.info('userEmail.email', userEmail)
            console.info('userphoneNumber.phoneNumber', userphoneNumber)

            /* console.info('userEmail.email',userEmail.email)
             console.info('userphoneNumber.phoneNumber', userphoneNumber.phoneNumber)
 */
            let transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'tchantio@gmail.com',
                    pass: 'aghvhkhydeelxyjt'
                },
                tls: {
                    rejectUnauthorized: false
                }
            })

            let otpCodeToSend = 0;
            otpCodeToSend = otpGenerator(4);

            const resetTryCount = await CustomOtp.find({ email: userEmail.email, phoneNumber: userphoneNumber.phoneNumber });

            if (resetTryCount.length < 4) {

                console.log('resetTryCount.length', resetTryCount.length)

                const otpRequest = new CustomOtp({
                    otpCode: otpCodeToSend,
                    email: userEmail.email,
                    phoneNumber: userphoneNumber.phoneNumber,
                    resetTryArray: [
                        {
                            email: userEmail.email,
                            phoneNumber: userphoneNumber.phoneNumber,
                        }
                    ],
                    //resetTryCount: { type: Number },
                })

                otpRequest.save();

                let mailOptions = {
                    from: "tchantio@gmail.com",
                    to: `${userEmail.email}`,
                    subject: "reset Your Password",
                    text: `Your code is ${otpCodeToSend}`
                }

                transporter.sendMail(mailOptions, function (err, success) {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log("email")
                    }
                })

                return res.status(200).json({ message: "User exist", userName: userEmail.userName });

            } else {
                console.log('c tropppppppppp')
                return res.status(200).json({ message: "Please contact our Service", userName: userEmail.userName });
            }


        }
        if (!checkifEmailExist) {
            return res.status(200).json({ message: `A User with this Email don't exist! ` });
        }
        if (!checkifPhoneNumberExist) {
            return res.status(200).json({ message: `A User with this Phone Number don't exist! ` });
        }
        /*if(userEmail && !userphoneNumber){
            return res.status(200).json({ message: `This Phone Number doesn't match to `});
        }
        if(!userEmail && userphoneNumber){
            return res.status(200).json({ message: `This Email doesn't match to`});
        }*/

    } catch (err) {
        console.log(err)
    }
}