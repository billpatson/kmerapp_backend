const TaxiDriverLocation = require("../../../models/taxiCar/taxiCarDriver");
const UsersLocation = require("../../../models/users/userLocation");
const { BookTaxiCar } = require("../../../models/taxiCar/bookTaxiCar");
const { SaveTaxiBooking } = require("../../../models/taxiCar/bookTaxiCar");
const getTime = require("../../../DbHelpers/getTimebetween2Coordinates");
const UUID = require('uuid-int');



exports.toto= async (req, res, next) => {
  try {
    send.json('bjr billy')
  } catch (error) {
    
  }
}

exports.saveTaxiDriverLocation = async (req, res, next) => {
  try {
    const _userId = req.body.userId;
    if (await TaxiDriverLocation.findOne({ userId: _userId })) {
      /*const error = new error(`An Account with the Name ${userName} already exists`);
      error.statusCode = 409;
      throw error;*/
      res.send("Taxi Driver Location already saved!!");

    } else {
      const taxiDriversLocation = new TaxiDriverLocation(req.body);
      taxiDriversLocation.save();
      //res.send("Taxi Driver Location saved!!");
    }

  } catch (err) {
    next(err);

  }
};




exports.trackTaxiDriver = async (req, res, next) => {


  console.info('mamaammamamamam', req.body)

  const trackDriver = await SaveTaxiBooking.find({ taxiPassengerUserID: req.body.passengerUserID, taxiDriverUserID: req.body.taxiDriverUserID, taxiBookingID: req.body.taxiBookingID },);

  if (Object.keys(trackDriver).length !== 0) {
    console.info('heheheheehhehe trackDriver', trackDriver);

    res.status(200).json({ trackDriverDataDetails: trackDriver })
  }



}

exports.saveTaxiBookingDetails = async (req, res, next) => {

  console.log('molahhhhhhhhhhh molahhhhhhhhhhh molahhhhhhhhhhh molahhhhhhhhhhh molahhhhhhhhhhh')


  const savedTaxiBooking = new SaveTaxiBooking({
    taxiDriverUserID: req.body.taxiDriverUserID,
    taxiPassengerUserID: req.body.taxiPassengerUserID,
    taxiPassengerData: req.body.taxiPassengerData,
    taxiDriverData: req.body.taxiDriverData,
    taxiBookingDetails: req.body.taxiBookingDetails,
    taxiModeType: req.body.taxiModeType, // Mototaxi  or TaxiCar,
    taxiBookingID: req.body.taxiBookingID,
    taxiBookingStatus: 'driverIsOnTheWay',
    taxiDriverDataPositionData: req.body.taxiDriverDataPositionData
  });

  savedTaxiBooking.save();
  res.send("Taxi Booking saved!!");

}
exports.confirmTaxiCarBookingsRequest = async (req, res, next) => {

  console.log('hello')

  BookTaxiCar.updateOne({ _id: req.params.id, taxiBookingID: req.body.taxiBookingID }, { ...req.body, _id: req.params.id })
    .then(() => {
      res.status(200).json({ message: 'Objet modifié !' })

    })
    .catch(error => res.status(400).json({ error }));

  const taxiCarBookings = await BookTaxiCar.find({ _id: req.params.id });

  console.info('totot taxiCarBookings passengerData', taxiCarBookings[0].passengerData)
  console.info('totot taxiCarBookings taxiDriverData', taxiCarBookings[0].taxiDriverData)

}
exports.cancelLookingForTaxiCar = async (req, res, next) => {
  try {
    console.info('cancelLookingForTaxiCar req.params.id=', req.params.id)

    BookTaxiCar.deleteOne({ _id: req.params.id, bookingStatus: 'pending' })//,userId:req.body.userId,bookingStatus:'pending'})
      .then(() => res.status(200).json({ message: 'Taxi Car booking successfully cancel' }))
      .catch(error => res.status(400).json({ error }));

  } catch (err) {
    next(err);
  }
}
exports.newIncomingsTaxiCarBookingsRequest = async (req, res, next) => {
  try {
    const economicBookings = await BookTaxiCar.find({ bookingStatus: 'pending', resquestType: 'economic' });
    const depotBookings = await BookTaxiCar.find({ bookingStatus: 'pending', resquestType: 'depot' });
    const courseBookings = await BookTaxiCar.find({ bookingStatus: 'pending', resquestType: 'course' });

    if (economicBookings.length === 0 && depotBookings.length === 0 && courseBookings.length === 0) {

      return res.json({ taxiCarBookingRequests: 'no Data' })
    } else {

      return res.status(200).json({
        success: true,
        economicCount: economicBookings.length,
        economicData: economicBookings,

        depotCount: depotBookings.length,
        depotData: depotBookings,

        courseCount: courseBookings.length,
        courseData: courseBookings,
      });
    }

  } catch (err) {


  }
}


exports.getSearchForTaxiCarPassenger_economic = async (req, res, next) => {
  try {
    let bookingData = {};
    BookTaxiCar.find({ resquestType: 'economic', bookingStatus: 'pending' })
      .then((economicPassenger) => {
        if (!economicPassenger) {
          bookingData.count = 0;
          bookingData.data = [];
          res.status(200).json({ TaxiCarPassengerBookingData: bookingData })
        } else {
          bookingData.count = economicPassenger.length;
          bookingData.data = economicPassenger;
          /*console.info('bookingData.count', bookingData.count)
          console.info('bookingData.count', bookingData)*/

          res.status(200).json({ TaxiCarPassengerBookingData: bookingData })
        }

      })
      .catch((err) => res.status(401).json({ errorMessage: err }))

  } catch (err) {
    next(err);
  }
}
exports.getSearchForTaxiCarPassenger_depot = async (req, res, next) => {
  try {
    let bookingData = {};
    BookTaxiCar.find({ resquestType: 'depot', bookingStatus: 'pending' })
      .then((depotPassenger) => {
        bookingData.count = depotPassenger.length;
        bookingData.data = depotPassenger;
        res.status(200).json({ TaxiCarPassengerBookingData: bookingData })

      })
      .catch((err) => res.status(401).json({ errorMessage: err }))
  } catch (err) {
    next(err);
  }
}
exports.getSearchForTaxiCarPassenger_course = async (req, res, next) => {
  try {
    let bookingData = {};
    BookTaxiCar.find({ resquestType: 'course', bookingStatus: 'pending' })
      .then((coursePassenger) => {
        bookingData.count = coursePassenger.length;
        bookingData.data = coursePassenger;
        res.status(200).json({ TaxiCarPassengerBookingData: bookingData })
      })
      .catch((err) => res.status(401).json({ errorMessage: err }))
  } catch (err) {
    next(err);

  }
}
exports.checkIfTaxicarPassengerRequestAccepted = async (req, res, next) => {

  try {
    console.log('bjr billy');
    console.log('bjr billy req.body.requestBookingId', req.body.requestBookingId);

    const passengerBooking = await BookTaxiCar.find({ bookingStatus: 'Booking confirmed', _id: req.body.requestBookingId });

    console.log('typeof(passengerBooking)====', typeof (passengerBooking))
    console.log('Object.keys(passengerBooking).length =======', Object.keys(passengerBooking).length)
    if (Object.keys(passengerBooking).length === 0) {
      console.log('bjr mr')
      return res.json({ taxiDriverData: 'no Data' })
      //console.log('bjr mr')
    } else {
      console.info('checkIfTaxicarPassengerRequestAccepted checkIfTaxicarPassengerRequestAccepted checkIfTaxicarPassengerRequestAccepted =', passengerBooking);
      //console.log('sudo passengerBooking.taxiBookingID passengerBooking.taxiBookingID',passengerBooking.taxiBookingID )
      return res.status(200).json({
        taxiBookingData: passengerBooking
        //taxiDriverData: passengerBooking
      })
    }


  } catch (err) {

    console.log('ERR checkIfTaxicarPassengerRequestAccepted', err)
  }
}

exports.lookingForTaxiCar = async (req, res, next) => {
  try {

    // number  0 <= id <=511
    const id = 0;

    const generator = UUID(id);
    const uuid = generator.uuid();

    const bookingTaxiCar = new BookTaxiCar({
      userId: req.body.userId,
      taxiDriverData: { data: 'no data' },
      taxiDriverUserID: req.body.taxiDriverUserID,
      taxiPassengerUserID: req.body.passengerData._id,
      passengerData: req.body.passengerData,
      bookingStatus: 'pending',
      resquestType: req.body.resquestType,
      taxiBookingDetails: req.body.taxiBookingDetails,
      taxiBookingID: uuid,
      updatedAt: '',
    });

    bookingTaxiCar.save();

    /*console.log('bookingTaxiCar',bookingTaxiCar);
    console.log('bookingTaxiCar ID',bookingTaxiCar._id);*/

    console.log('my uuid uuid=', uuid)
    console.log('my taxiBookingID taxiBookingID=', bookingTaxiCar.taxiBookingID)

    res.send({ requestId: bookingTaxiCar._id, lookingFortaxiCarStatus: "pending", taxiBookingID: bookingTaxiCar.taxiBookingID })

    // }

    console.log('taraaaaa masssa', bookingTaxiCar)

  } catch (err) {
    next(err);

  }
}

exports.getNearTaxiDriverLocation = async (req, res, next) => {

  try {

    const _userId = req.body.userId;
    if (await UsersLocation.findOne({ userId: _userId })) {
      UsersLocation.updateOne(
        { userId: _userId },
        { ...req.body, userId: _userId }
      ).then(() => {
        TaxiDriverLocation.aggregate([
          {
            $geoNear: {
              near: {
                type: "Point",
                coordinates: [
                  parseFloat(req.body.longitude),
                  parseFloat(req.body.latitude),
                ],
              },
              maxDistance: 500000, //10 km=10000
              distanceField: "dist.calculated",
              spherical: true,
            },
          }
        ]).then(function (nearTaxiCarDrivers) {
          /*console.log('bon 2')
          console.info('nearTaxiCarDrivers', nearTaxiCarDrivers);
          console.info('nearTaxiCarDrivers[0]', nearTaxiCarDrivers[0].coordinate.coordinates);
          console.info('lol nearTaxiCarDrivers[0].coordinate.coordinates[0]', nearTaxiCarDrivers[0].coordinate.coordinates[0]);*/

          //taxiCarDriversLocation represente les coordonnees geographiques extraits de tous les taxi Driver qui sont aux alentours
          const taxiCarDriversLocation = Object.keys(nearTaxiCarDrivers).reduce((result, key) => {
            return result.concat(nearTaxiCarDrivers[key].coordinate);
          }, []);
          //console.info('taxiCarDriversLocation', taxiCarDriversLocation);
          let nearTaxiCarDriversDataToSend = [];

          for (let i = 0; i < nearTaxiCarDrivers.length; i++) {
            nearTaxiCarDriversDataToSend.push({
              TaxiCarDrverName: '',
              TaxiDriverLocation: { latitude: nearTaxiCarDrivers[i].coordinate.coordinates[1], longitude: nearTaxiCarDrivers[i].coordinate.coordinates[0] },
              // TimeToPassenger:getTime.getTimeBetween2Coordinates(nearTaxiCarDrivers[i].coordinate.coordinates[1],nearTaxiCarDrivers[i].coordinate.coordinates[0],req.body.latitude,req.body.longitude)
            })
            //console.info(`taxiCarDriversLocation [${i}] = `, taxiCarDriversLocation[i])
          }
          //console.info('nearTaxiCarDriversDataToSend', nearTaxiCarDriversDataToSend)

          res.send({ nearTaxiCarDriversDatas: nearTaxiCarDriversDataToSend });
        })

      })
        .catch((error) => res.status(400).json({ error }));
    }
  } catch (err) {
    next(err);
  }
}


exports.getNearTaxiDriverLocationss = async (req, res, next) => {

  try {
    const _userId = req.body.userId;
    if (await TaxiDriverLocation.findOne({ userId: _userId })) {
      TaxiDriverLocation.updateOne(
        { userId: _userId },
        { ...req.body, userId: _userId }
      ).then(() => {

      })
        .catch((error) => res.status(400).json({ error }));
    }
  } catch (err) {
    next(err);
  }
}


//get Taxi Driver Location

exports.taxiDriverLocation = async (req, res, next) => {
  try {
    const _userId = req.body.userId;

    //console.log('req.body', req.body)
    /*const { userName } = req.body;
    console.log("req.body.userName", req.body.userName);*/

    if (await TaxiDriverLocation.findOne({ userId: _userId })) {
      TaxiDriverLocation.updateOne(
        { userId: _userId },
        { ...req.body, userId: _userId }
      )
        .then(() =>
          UsersLocation.aggregate([
            {
              $geoNear: {
                near: {
                  type: "Point",
                  coordinates: [
                    parseFloat(req.body.longitude),
                    parseFloat(req.body.latitude),
                  ],
                },
                maxDistance: 500000, //10 km=10000
                distanceField: "dist.calculated",
                spherical: true,
              },
            },
          ]).then(function (allNearTaxiPassengerData) {
            //get nearby Taxi Passenger

            const taxiPassengerLocation = Object.keys(
              allNearTaxiPassengerData
            ).reduce((result, key) => {
              return result.concat(allNearTaxiPassengerData[key].coordinate);
            }, []);

            const taxiPassengerDatas = [];

            allNearTaxiPassengerData.forEach(function (pp) {
              taxiPassengerDatas.push({
                taxiPassengerName: pp.userName,
                taxiPassengerId: pp.userId,
                coordinate: {
                  latitude: pp.coordinate.coordinates[1],
                  longitude: pp.coordinate.coordinates[0],
                },
                distanceToDriver: pp.dist,
              });
            });

            const allNearTaxiPassengerPosition = [];
            for (let i = 0; i < taxiPassengerLocation.length; i++) {
              allNearTaxiPassengerPosition.push({
                coordinates: {
                  latitude: taxiPassengerLocation[i].coordinates[1],
                  longitude: taxiPassengerLocation[i].coordinates[0],
                },
              });
            }
            res.status(201).json({ NearUsersLocation: taxiPassengerDatas });
          })
        )
        .catch((error) => res.status(400).json({ error }));
    } else {
      const taxiDriversLocation = new TaxiDriverLocation(req.body);
      taxiDriversLocation.save();
    }
  } catch (err) {
    next(err);
  }
};
