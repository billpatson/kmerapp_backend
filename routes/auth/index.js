const express = require("express");
const router = express.Router();
const loginController = require('../../controllers/auth/login');
const registerController = require('../../controllers/auth/register');
const resetPasswordController = require('../../controllers/auth/resetPassword');
router.post("/login",loginController.login)
router.post('/register', registerController.register);
router.post('/checkIfEmailandPhoneNumberExist', resetPasswordController.checkIfEmailandPhoneNumberExist);
router.post('/sendOtpCode', resetPasswordController.sendOtpCode);
router.put('/resetPassword', resetPasswordController.resetPassword);


module.exports = router;
