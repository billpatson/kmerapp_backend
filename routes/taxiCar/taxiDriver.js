const express = require("express");
const router = express.Router();
const auth = require('../../middelware/auth');

const taxiDriverController = require('../../controllers/taxiCar/driver');

router.post("/lookForDriver",auth,taxiDriverController.taxiDriverLocation);
router.post("/saveTaxiCarDriverLocation",auth,taxiDriverController.saveTaxiDriverLocation);
router.post("/checkIfTaxicarPassengerRequestAccepted",auth,taxiDriverController.checkIfTaxicarPassengerRequestAccepted);
router.post("/getNearTaxiDriverLocation",auth,taxiDriverController.getNearTaxiDriverLocation);
router.post("/lookingForTaxiCarDriver",auth,taxiDriverController.lookingForTaxiCar);
router.delete("/cancelLookingForTaxiCarDriver/:id",auth,taxiDriverController.cancelLookingForTaxiCar);
router.post("/searchForTaxiCarPassenger_economic",auth,taxiDriverController.getSearchForTaxiCarPassenger_economic);
router.post("/searchForTaxiCarPassenger_course",auth,taxiDriverController.getSearchForTaxiCarPassenger_course);
router.post("/searchForTaxiCarPassenger_depot",auth,taxiDriverController.getSearchForTaxiCarPassenger_depot);
router.post("/newIncomingsTaxiCarBookingsRequest",auth,taxiDriverController.newIncomingsTaxiCarBookingsRequest);
router.put("/confirmTaxiCarBookingsRequest/:id",auth,taxiDriverController.confirmTaxiCarBookingsRequest);
router.post("/saveTaxiBookingDetails",auth,taxiDriverController.saveTaxiBookingDetails);
router.post("/trackTaxiDriver",auth,taxiDriverController.trackTaxiDriver);

router.get("/toto",taxiDriverController.toto);


module.exports = router;
