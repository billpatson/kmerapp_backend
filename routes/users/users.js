const express = require("express");
const router = express.Router();
const userController = require('../../controllers/users/users');
const auth = require('../../middelware/auth');

router.post("/getAllUsers",auth,userController.getAllUsers)
router.post("/saveOrUpdateUsersLocation",auth,userController.saveOrUpdateUsersLocation)

module.exports = router;
